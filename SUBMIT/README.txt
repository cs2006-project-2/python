# INSTALLATION

To run our code you will require:

- Pandas
- Numpy
- Matplotlib

Recommended isntallation through Anaconda

https://docs.anaconda.com/anaconda/install/

#CONTENTS

Code

- Jypter Notebook (Notebook.ipynb)
- Complimentary Python files

Data

- Census 2011 csv
- Refined Census 2011 csv
- Other supplied files

Reports

- Notebook PDF
- Notebook HTML
- CONTRIBUTIONS.txt