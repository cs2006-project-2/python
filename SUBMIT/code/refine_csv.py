# check the file has not been corrupted
# check the data has no duplicates
# check the values are of expected types
# checks there are no logical contradictions in the file

import sys
import csv
from pathlib import Path

ids = set()
old_filepath = ""
new_filepath = ""


def csv_read(filepath):
    deletions = 0
    count = 0
    input_file = csv.DictReader(open(filepath))
    rows = list(input_file)

    for row in rows:
        count += 1
        if (not check_contradictions(row)) \
                or (not check_duplicates(row)) \
                or (not check_expected_values(row)):
            rows.remove(row)
            deletions += 1
            print("Removed", deletions, "/", count, "rows from new file.")
    return rows


def csv_write(rows):
    f = open(new_filepath, "w")
    writer = csv.DictWriter(
        f, fieldnames=['Person ID',
                       'Region',
                       'Residence Type',
                       'Family Composition',
                       'Population Base',
                       'Sex',
                       'Age',
                       'Marital Status',
                       'Student',
                       'Country of Birth',
                       'Health',
                       'Ethnic Group',
                       'Religion',
                       'Economic Activity',
                       'Occupation',
                       'Industry',
                       'Hours worked per week',
                       'Approximated Social Grade'
                       ])
    writer.writeheader()
    writer.writerows(rows)
    f.close()
    print("New file ", new_filepath, "written.")


def input_val():
    if len(sys.argv) != 3:
        print("Usage error, Program has two arguments.")
        print("Usage: refine_csv.py <old_filename> <new_filename>")
        exit(0)
    else:
        f = Path(sys.argv[2].strip())
        if not f.is_file():
            print("File", sys.argv[1], "cannot be found.")
            exit(0)
        else:
            global old_filepath, new_filepath
            old_filepath = sys.argv[1]
            new_filepath = sys.argv[2]


def check_duplicates(row):
    global ids
    id_code = row['Person ID']
    if id_code not in ids:
        ids.add(id_code)
        return True
    else:
        return False


# EXPECTED VALUES:
# Region: E12000001 - E12000009 OR W92000004
# Family Composition: 1 - 6 OR -9
# Population Base: 1 - 3
# Sex: 1 - 2
# Age: 1 - 8
# Marital status: 1 - 5
# Student: 1 - 2
# Country of Birth: 1 - 2 or -9
# Health: 1 - 5 or -9
# Ethnic group: 1 - 5 or -9
# Religion: 1 - 9 or -9
# Economic activity: 1 - 9 or -9
# Occupation: 1 - 9 or -9
# Industry: 1 - 12 or -9
# Hours worked per week: 1 - 4 or -9
# Approximated social grade: 1 - 4 or -9
def check_expected_values(row):
    r = {"E12000001", "E12000002", "E12000003", "E12000004", "E12000005", "E12000006",
         "E12000007", "E12000008", "E12000009", "W92000004"}
    res = set('CH')
    fc = {1, 2, 3, 4, 5, 6, -9}
    pb = {1, 2, 3}
    s = {1, 2}
    a = {1, 2, 3, 4, 5, 6, 7, 8}
    ms = {1, 2, 3, 4, 5}
    st = {1, 2}
    cob = {1, 2, -9}
    h = {1, 2, 3, 4, 5, -9}
    eg = {1, 2, 3, 4, 5, -9}
    re = {1, 2, 3, 4, 5, 6, 7, 8, 9, -9}
    ea = {1, 2, 3, 4, 5, 6, 7, 8, 9, -9}
    o = {1, 2, 3, 4, 5, 6, 7, 8, 9, -9}
    i = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, -9}
    hwpw = {1, 2, 3, 4, -9}
    asg = {1, 2, 3, 4, -9}

    r_b = not row["Region"] in r
    res_b = not row["Residence Type"] in res
    fc_b = not int(row["Family Composition"]) in fc
    pb_b = not int(row["Population Base"]) in pb
    s_b = not int(row["Sex"]) in s
    a_b = not int(row["Age"]) in a
    ms_b = not int(row["Marital Status"]) in ms
    st_b = not int(row["Student"]) in st
    cob_b = not int(row["Country of Birth"]) in cob
    h_b = not int(row["Health"]) in h
    eg_b = not int(row["Ethnic Group"]) in eg
    re_b = not int(row["Religion"]) in re
    ea_b = not int(row["Economic Activity"]) in ea
    o_b = not int(row["Occupation"]) in o
    i_b = not int(row["Industry"]) in i
    hwpw_b = not int(row["Hours worked per week"]) in hwpw
    asg_b = not int(row["Approximated Social Grade"]) in asg

    if r_b or res_b or fc_b or pb_b or s_b or a_b or ms_b or st_b or cob_b \
            or h_b or eg_b or re_b or ea_b or o_b or i_b or hwpw_b or asg_b:
        return False
    else:
        return True


# CONTRADICTIONS:
# NOT SINGLE AND 0 - 15 YEARS OF AGE
# Marital status NOT 1 AND Age 1
#
# MALE HEAD OF FAMILY AND NOT MALE
# Family Composition 4 AND Sex NOT 1
#
# FEMALE HEAD OF FAMILY AND NOT FEMALE
# Family Composition 5 AND Sex Not 2
#
# ECONOMICAL STUDENT AND NOT STUDENT
# Economic Activity 4 OR 6 AND NOT Student 1
#
# 0 - 15 YEARS OF AGE AND NOT STUDENT
# NOT Student 1 AND Age 1
#
# MARRIED BUT NOT IN A FAMILY
# Marital status 2 AND Family composition 1
#
# IN A MARRIAGE BUT NOT MARRIED
# Marital status 1 AND
def check_contradictions(row):
    if not row['Marital Status'] == 1 and row['Age'] == 1:
        # print("1st case")
        return False
    elif row['Family Composition'] == 4 and not row['Sex'] == 1:
        # print("2nd case")
        return False
    elif row['Family Composition'] == 5 and not row['Sex'] == 2:
        # print("3rd case")
        return False
    elif row['Age'] == 1 and not row['Student'] == 1:
        # print("4th case")
        return False
    elif (row['Economic Activity'] == 4 or row['Economic Activity'] == 6) and not row['Student'] == 1:
        # print("5th case")
        return False
    elif row['Marital Status'] == 2 and row['Family Composition'] == 1:
        # print("6th case")
        return False
    else:
        return True


def run():
    csv_write(csv_read(old_filepath))


print("Starting...")
input_val()
run()
print("...Done")
