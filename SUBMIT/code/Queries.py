

def queryByVar(df, query, variable):
    """
    Runs the query using a copy of the dataframe, groups result of query by input variable.
    Prints total result of query and returns the 'group by variable' table.

    :param df: Dataset to query
    :type df: Pandas Dataframe
    :param query: The query to execute
    :type query: String
    :param variable: Variable to 'group by' the result of the query
    :type variable: String
    
    """

    df = df.copy(deep = True) # Deep copy to avoid affecting original dataframe

    # Replace blank spaces with '_' as required
    df.columns = [column.replace(" ", "_") for column in df.columns] 
    variable   = variable.replace(" ", "_")

    df.query(query, inplace = True) # Performs the query on the dataframe

    x = df.groupby(variable)        # Groups the data by the input variable

    # Output total reult and return the 'group by variable' table
    print("\nQuery condition met", len(df.index), "times - returning breakdown\n")
    return x.size()



def compareQueryResults(df, query1, query2):
    """
    Runs the two input queries on the data and compares the result. 
    Prints output appropriate in regards to whether the res

    :param df: Dataset to query
    :type df: Pandas Dataframe
    :param query1: The first query to execute
    :type query1: String
    :param query2: The second query to execute
    :type query2: String
    """

    df1 = df.copy(deep = True) # Create first copy of the dataframe to be queried

    df1.columns = [column.replace(" ", "_") for column in df.columns]

    df2 = df1.copy(deep = True) # Create copy of the copy to avoid replacing spaces again

    df1.query(query1, inplace = True) # Performs the query on the dataframe 1

    df2.query(query2, inplace = True) # Performs the query on the dataframe 2

    # Get total result values of each query
    count1 = len(df1.index)
    count2 = len(df2.index)

    # Output results
    print("Query 1:", count1, "Results\n")
    print("Query 2:", count2, "Results\n")


    if(count1 == count2):   # If no discrepancies - display message

        print("There does not seem to be any discrepancies between the two queries.\n")

    else:                   # If there are discrepancies - calculate by how much and output to user

        num = count1 - count2 if count1 > count2 else count2 - count1
        closer = "Results" if num > 1 else "Result"
        print("There is a descripancy of about", num, closer, "\n")

