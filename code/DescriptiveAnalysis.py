def totalRecords(df):
    """
    Produces the total number of records found in a given dataset. Prints result.
    
    :param df: Dataset to analyse
    :type df: Pandas Dataframe
    """

    index = df.index
    records = len(index)

    # Print the total number of records in the dataset
    print("Total Number of records =", records, "\n")

def varTypes(df):
    """
    Produces a list of all variables in the dataframe alongside their data types. Prints result.
    
    :param df: Dataset to analyse
    :type df: Pandas Dataframe
    """

    # Print the type of each variable in the dataset
    print("Type of each Variables")
    print(df.dtypes)

def occurancesOfValues(df, idColumn):
    """
    for each variable except The ID, produces all different values that they take,
    and the number of occurrences for each value. Prints result.
    
    :param df: Dataset to analyse
    :type df: Pandas Dataframe
    :param idColumn: Var name of ID column
    :type idColumn: String
    """    

    dfWithoutPID = df.loc[:, df.columns != idColumn] # Effectively filters out the ID column for this analysis

    # prints all values the variables take and their number of occurances
    for var in dfWithoutPID:
        
        x = dfWithoutPID.groupby(var)

        print("------------------")
        print(x.size())
        print("")
        

def convertInterpretation(df, idColumn, varDict):
    """
    Changes the alpha neumeric codes from a dataset into their textual interpretations.

    :param df: Dataset to convert
    :type df: Pandas Dataframe
    :param idColumn: Var name of ID column
    :type idColumn: String
    :param varDict: Dictionary containing the alpha neumeric codes and textual representations of them
    :type varDict: Dictionary
    :rtype: Pandas Dataframe
    """

    print("Starting...")

    for col in df: # for every column in the dataframe

        if col == idColumn: # Ignore ID Column
            print("Ignoring", col)
            continue

        elif col in varDict: # if it is in the dictionary

            print(col, "Found - Processing...")

            df[col] = df[col].apply(str)        # Convert column to String
            v = varDict.get(col)                # Get Sub Dictionary for this variable
            i = 0                               # Dataframe index
        
            # For every element of data in the column, replace it with matching dictionary value
            for element in df[col]:

                df.at[i, col] = v.get(element)
                i += 1

        else: # this column is not in the dictionary - prints simple error message
            print(col, "not found in dictionary!")

    print("...Done")
    return df
